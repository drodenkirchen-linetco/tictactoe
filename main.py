# Author: David Rodenkirchen
# E-Mail: Typ260@googlemail.com
# Created: 05.11.2020

import pygame


def main():
    # Initialisiere PyGame Engine
    pygame.init()
    # Setze Fenstertitel
    pygame.display.set_caption("Tic Tac Toe")
    # Initialisere Fenster
    screen = pygame.display.set_mode((300, 300))
    # Lade Spielfeld Grafik und zeichne sie ins Fenster
    spielfeld = pygame.image.load("assets/spielfeld.png")
    screen.blit(spielfeld, (0, 0))
    pygame.display.flip()
    # Lade X, O und Unentschieden
    x_gfx = pygame.image.load("assets/x.png")
    x_gfx_win = pygame.image.load("assets/x_win.png")
    o_gfx = pygame.image.load("assets/o.png")
    o_gfx_win = pygame.image.load("assets/o_win.png")
    draw_gfx = pygame.image.load("assets/draw.png")
    # Game Loop Trigger und so
    spiel_laueft = True
    # Datensatz für Spielfeld
    spielfeld_daten = [
                        ["leer", 15, 15], ["leer", 115, 15], ["leer", 215, 15],
                        ["leer", 15, 115], ["leer", 115, 115], ["leer", 215, 115],
                        ["leer", 15, 215], ["leer", 115, 215], ["leer", 215, 215]
    ]
    pygame.display.flip()
    # Variablen
    input_annehmen = True
    spieler_eins = ["X", x_gfx]
    spieler_zwei = ["O", o_gfx]
    spieler_am_zug = spieler_eins[0]

    def berechne_geklicktes_feld(_pos):
        x = _pos[0]
        y = _pos[1]
        if x < 95 and y < 95:
            return 0
        elif x < 195 and y < 95:
            return 1
        elif x < 300 and y < 95:
            return 2
        elif x < 95 and y < 195:
            return 3
        elif x < 195 and y < 195:
            return 4
        elif x < 300 and y < 195:
            return 5
        elif x < 95 and y < 300:
            return 6
        elif x < 195 and y < 300:
            return 7
        elif x < 300 and y < 300:
            return 8

    def ueberpruefe_sieger():
        sieg_konditionen = [
            # Vertikal
            spielfeld_daten[0][0] + spielfeld_daten[1][0] + spielfeld_daten[2][0],
            spielfeld_daten[3][0] + spielfeld_daten[4][0] + spielfeld_daten[5][0],
            spielfeld_daten[6][0] + spielfeld_daten[7][0] + spielfeld_daten[8][0],
            # Diagonal
            spielfeld_daten[0][0] + spielfeld_daten[3][0] + spielfeld_daten[6][0],
            spielfeld_daten[1][0] + spielfeld_daten[4][0] + spielfeld_daten[7][0],
            spielfeld_daten[2][0] + spielfeld_daten[5][0] + spielfeld_daten[8][0],
            # Quer
            spielfeld_daten[0][0] + spielfeld_daten[4][0] + spielfeld_daten[8][0],
            spielfeld_daten[2][0] + spielfeld_daten[4][0] + spielfeld_daten[6][0]

        ]
        gesamtwert_aller_sets = 0
        for sieg_kondition in sieg_konditionen:
            gesamtwert_aller_sets += len(set(sieg_kondition))
            if len(set(sieg_kondition)) == 1:
                return "Win"
        if gesamtwert_aller_sets == 16:
            return "Tie"
        return False

    def sieger_ausrufen(_spieler_am_zug):
        if _spieler_am_zug == spieler_eins[0]:
            screen.blit(x_gfx_win, (0, 0))
        elif _spieler_am_zug == spieler_zwei[0]:
            screen.blit(o_gfx_win, (0, 0))
        else:
            screen.blit(draw_gfx, (0, 0))
        pygame.display.flip()

    while spiel_laueft:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP and input_annehmen:
                pos = pygame.mouse.get_pos()
                auswahl = berechne_geklicktes_feld(pos)
                if spielfeld_daten[auswahl][0] == spieler_eins[0] or spielfeld_daten[auswahl][0] == spieler_zwei[0]:
                    print("Dieses Feld ist bereits belegt!")
                else:
                    spielfeld_daten[auswahl][0] = spieler_am_zug
                    if spieler_am_zug == spieler_eins[0]:
                        screen.blit(spieler_eins[1], (spielfeld_daten[auswahl][1], spielfeld_daten[auswahl][2]))
                        if ueberpruefe_sieger() == "Win":
                            sieger_ausrufen(spieler_am_zug)
                            input_annehmen = False
                        elif ueberpruefe_sieger() == "Tie":
                            sieger_ausrufen("")
                            input_annehmen = False
                        spieler_am_zug = spieler_zwei[0]
                    else:
                        screen.blit(spieler_zwei[1], (spielfeld_daten[auswahl][1], spielfeld_daten[auswahl][2]))
                        if ueberpruefe_sieger() == "Win":
                            sieger_ausrufen(spieler_am_zug)
                            input_annehmen = False
                        elif ueberpruefe_sieger() == "Tie":
                            sieger_ausrufen("")
                            input_annehmen = False
                        spieler_am_zug = spieler_eins[0]
            pygame.display.flip()
            if event.type == pygame.QUIT:
                spiel_laueft = False


if __name__ == '__main__':
    main()
